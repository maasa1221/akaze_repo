import cv2
import numpy as np


cap = cv2.VideoCapture("src/hand.mov")
ret, frame0 = cap.read()
img1 = cv2.resize(frame0, (int(frame0.shape[1]/4), int(frame0.shape[0]/4)))
img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)

akaze = cv2.AKAZE_create()       
                    
kp1, des1 = akaze.detectAndCompute(img1, None)

cap = cv2.VideoCapture("src/hand.mov")
p_angle_result=[]
p_angle_new=[]
p_angle_base=[]

count=0
p_angle_result=np.zeros(20)
while True:
    ret, frame = cap.read()
    if frame is None:
        break
    frame = cv2.resize(frame, (int(frame.shape[1]/4), int(frame.shape[0]/4)))
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    kp2, des2 = akaze.detectAndCompute(frame, None)


    bf = cv2.BFMatcher()

    matches = bf.knnMatch(des1, des2, k=2)

    p_angle_new=[]

    ratio = 0.5
    p_pt_all=0
    good = sorted(matches, key = lambda x : x[1].distance)
    for m, n in matches:
        if m.distance < ratio * n.distance:
            good.append([m])


    img3 = cv2.drawMatchesKnn(img1, kp1, frame, kp2, good[:30], None, flags=2)

    p_angle_new=np.array(p_angle_new)
    p_angle_base=np.array(p_angle_base)

    for i, lm in enumerate(good[:20]):

        bgr = tuple(i >> j & 1 for j in range(3))
        color1 = tuple(map(lambda x: x * 0xFF, bgr))
        color2 = tuple(map(lambda x: x * 0xC0, bgr))
        m = lm[0]
        p1 = tuple(map(int, kp1[m.queryIdx].pt))
        p2 = tuple(map(int, kp2[m.trainIdx].pt))
        cv2.circle(img3, p1, 5, color1, -1)
        cv2.circle(img3, (p2[0] + img1.shape[1], p2[1]), 5, color2, -1)
        p_pt=kp1[m.queryIdx].pt[0]
        p_angle=kp2[m.trainIdx].angle
        if p_angle>=300:
            p_angle=360-p_angle
        if count==0:
            p_angle_base=np.append(p_angle_base,p_angle)
        p_angle_new=np.append(p_angle_new,p_angle)

    
    count=+1
    p_angle_new=p_angle_new-p_angle_base
    p_angle_result=np.abs(p_angle_new)+p_angle_result

    p_angle_lotate=[]
    p_angle_lotate=np.array(p_angle_lotate)
    good=np.array(good[:20])
    for i, lm in enumerate(good[p_angle_result>10000]):
        m = lm[0]
        p_angle=kp2[m.trainIdx].angle
        if p_angle>=300:
            p_angle=360-p_angle
        p_angle_lotate=np.append(p_angle_lotate,p_angle)

cap = cv2.VideoCapture("src/hand.mov")  
while True:
    ret, frame = cap.read()
    if frame is None:
        break
    frame = cv2.resize(frame, (int(frame.shape[1]/4), int(frame.shape[0]/4)))
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    kp2, des2 = akaze.detectAndCompute(frame, None)
    bf = cv2.BFMatcher()
    matches = bf.knnMatch(des1, des2, k=2)
    p_angle_new=[]

    ratio = 0.5
    p_pt_all=0
    good = sorted(matches, key = lambda x : x[1].distance)
    for m, n in matches:
        if m.distance < ratio * n.distance:
            good.append([m])


    img3 = cv2.drawMatchesKnn(img1, kp1, frame, kp2, good[:30], None, flags=2)


    p_angle_lotate=[]
    p_angle_lotate=np.array(p_angle_lotate)
    good=np.array(good[:20])
    for i, lm in enumerate(good[p_angle_result>10000]):
        m = lm[0]
        p_angle=kp2[m.trainIdx].angle
        if p_angle>=300:
            p_angle=360-p_angle
        p_angle_lotate=np.append(p_angle_lotate,p_angle)


    
    
    cv2.imshow('img', img3)
    print(p_angle_lotate.mean())
    
    k = cv2.waitKey(1)
    if k == 27:
        break

cap.release()
cv2.destroyAllWindows()
